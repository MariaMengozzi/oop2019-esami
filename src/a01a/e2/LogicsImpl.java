package a01a.e2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class LogicsImpl implements Logics {
    
    private final int size;
    private final int sizeBoat;
    private List<Pair<Integer,Integer>> boat = new ArrayList<>();
    private Random rdm = new Random();
    private int countX = 0;
    private int countO = 0;
    

    public LogicsImpl(int size, int sizeBoat) {
        this.size = size;
        this.sizeBoat = sizeBoat;
        Pair<Integer,Integer> startBoat = new Pair<>(rdm.nextInt(size-1),rdm.nextInt(size-1));
        while (startBoat.getY()== size-1-sizeBoat) {
            startBoat = new Pair<>(rdm.nextInt(size-1),rdm.nextInt(size-1));
        }
        boat.add(startBoat);
        for (int i=1;i<sizeBoat;i++) {
            boat.add(new Pair<>(startBoat.getX()+i,startBoat.getY()));
        }
        System.out.println(boat);
    }

    @Override
    public boolean selected(int x, int y) {
        
        Pair<Integer,Integer> sel = new Pair<>(x,y);
        System.out.println(sel);
        if(boat.contains(sel)) {
            countX++;
            return true;
        }
        countO++;
        return false;
    }

    @Override
    public boolean closable() {
        return countX == sizeBoat || countO == 5;
    }

    @Override
    public boolean win() {
        return countX == sizeBoat;
    }

}
