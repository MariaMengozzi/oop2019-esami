package a01a.e2;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    
   
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Map<JButton, Pair<Integer,Integer>> jbs = new HashMap<>();
    private final Logics logics;

    public GUI(int size, int boat) { 
        this.logics = new LogicsImpl(size, boat);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
        JPanel panel = new JPanel(new GridLayout(size,size));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            Pair<Integer,Integer> pos = this.jbs.get(bt);
            String str = logics.selected(pos.getY(),pos.getX()) == true ? "X" : "O";
            bt.setText(str);
            bt.setEnabled(false);
            if(logics.closable()) {
                if(logics.win()) {
                    JOptionPane.showMessageDialog(null,"You Win");
                } else {
                    JOptionPane.showMessageDialog(null,"You Lose");
                }
                System.exit(1);
            }
        };
        for (int x=0;x<size;x++){
            for (int y=0;y<size;y++){
                //final JButton jb = new JButton("x"+String.valueOf(x)+" y"+String.valueOf(y));
                final JButton jb = new JButton(" ");
                jb.addActionListener(al);
                panel.add(jb);
                this.jbs.put(jb, new Pair<>(x,y));
            }
        } 
        this.setVisible(true);

    }
    
}
