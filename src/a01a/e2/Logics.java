package a01a.e2;

public interface Logics {

    boolean selected(int x, int y);
    boolean closable();
    boolean win();
}
