package a01a.e1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GraphFactoryImpl implements GraphFactory {

    @Override
    public <X> Graph<X> createDirectedChain(List<X> nodes) {
        return new Graph<X> () {

            @Override
            public Set<X> getNodes() {
                Set<X> out = nodes.stream().collect(Collectors.toSet());
                return Collections.unmodifiableSet(out);
            }

            @Override
            public boolean edgePresent(X start, X end) {
                return nodes.indexOf(end)==nodes.indexOf(start)+1;
            }

            @Override
            public int getEdgesCount() {
                return (nodes.size()-1);
            }

            @Override
            public Stream<Pair<X, X>> getEdgesStream() {
                List<Pair<X,X>> list = new ArrayList<>();
                for(int i=0;i<nodes.size();i++) {
                    list.add(new Pair<>(nodes.get(i),nodes.get(i+1)));
                }
                return list.stream();
            }
            
        };
        
    }

    @Override
    public <X> Graph<X> createBidirectionalChain(List<X> nodes) {
        return new Graph<X> () {

            @Override
            public Set<X> getNodes() {
                Set<X> out = nodes.stream().collect(Collectors.toSet());
                //System.out.println(out);
                return Collections.unmodifiableSet(out);
            }

            @Override
            public boolean edgePresent(X start, X end) {
                return nodes.indexOf(end)==nodes.indexOf(start)+1 || nodes.indexOf(end)==nodes.indexOf(start)-1;
            }

            @Override
            public int getEdgesCount() {
                return (nodes.size()-1)*2;
            }

            @Override
            public Stream<Pair<X, X>> getEdgesStream() {
                List<Pair<X,X>> list = new ArrayList<>();
                for(int i=0;i<nodes.size();i++) {
                    list.add(new Pair<>(nodes.get(i),nodes.get(i+1)));
                }
                
                for(int i=nodes.size()-1;i>0;i--) {
                    list.add(new Pair<>(nodes.get(i),nodes.get(i-1)));
                }
                
                return list.stream();
            }
            
        };
    }

    @Override
    public <X> Graph<X> createDirectedCircle(List<X> nodes) {
        return new Graph<X>() {

            @Override
            public Set<X> getNodes() {
                Set<X> out = nodes.stream().collect(Collectors.toSet());
                return Collections.unmodifiableSet(out);
            }

            @Override
            public boolean edgePresent(X start, X end) {
                return nodes.indexOf(end)==nodes.indexOf(start)+1 || nodes.indexOf(end)==0;
            }

            @Override
            public int getEdgesCount() {
                return nodes.size();
            }

            @Override
            public Stream<Pair<X, X>> getEdgesStream() {
                List<Pair<X,X>> list = new ArrayList<>();
                for(int i=0;i<nodes.size();i++) {
                    list.add(new Pair<>(nodes.get(i),nodes.get(i+1)));
                }
                list.add(new Pair<>(nodes.get(nodes.size()-1),nodes.get(0)));
                return list.stream();
            }
            
        };
    }

    @Override
    public <X> Graph<X> createBidirectionalCircle(List<X> nodes) {
        return new Graph<X>() {

            @Override
            public Set<X> getNodes() {
                Set<X> out = nodes.stream().collect(Collectors.toSet());
                return Collections.unmodifiableSet(out);
            }

            @Override
            public boolean edgePresent(X start, X end) {
                return Math.abs(nodes.indexOf(end)-nodes.indexOf(start))==1 || 
                        nodes.indexOf(end)==0 
                        || (nodes.indexOf(start)==0 && nodes.indexOf(end)==nodes.size()-1) ;
            }

            @Override
            public int getEdgesCount() {
                return nodes.size()*2;
            }

            @Override
            public Stream<Pair<X, X>> getEdgesStream() {
                List<Pair<X,X>> list = new ArrayList<>();
                for(int i=0;i<nodes.size();i++) {
                    list.add(new Pair<>(nodes.get(i),nodes.get(i+1)));
                }
                
                for(int i=nodes.size()-1;i>0;i--) {
                    list.add(new Pair<>(nodes.get(i),nodes.get(i-1)));
                }
                
                return list.stream();
            }
            
        };
    }

    @Override
    public <X> Graph<X> createDirectedStar(X center, Set<X> nodes) {
        return new Graph<X>() {

            @Override
            public Set<X> getNodes() {
                Set<X> out = nodes.stream().collect(Collectors.toSet());
                out.add(center);
                return Collections.unmodifiableSet(out);
            }

            @Override
            public boolean edgePresent(X start, X end) {
                return start!=end;
            }

            @Override
            public int getEdgesCount() {
                return nodes.size();
            }

            @Override
            public Stream<Pair<X, X>> getEdgesStream() {
                List<Pair<X,X>> list = new ArrayList<>();
                for(X x : nodes) {
                    list.add(new Pair<>(center,x));
                }
                
                return list.stream();
            }
            
        };
    }


    @Override
    public <X> Graph<X> createBidirectionalStar(X center, Set<X> nodes) {
        return new Graph<X>() {

            @Override
            public Set<X> getNodes() {
                Set<X> out = nodes.stream().collect(Collectors.toSet());
                out.add(center);
                return Collections.unmodifiableSet(out);
            }

            @Override
            public boolean edgePresent(X start, X end) {
                return start!=end;
            }

            @Override
            public int getEdgesCount() {
                return nodes.size()*2;
            }

            @Override
            public Stream<Pair<X, X>> getEdgesStream() {
                List<Pair<X,X>> list = new ArrayList<>();
                for(X x : nodes) {
                    list.add(new Pair<>(center,x));
                    list.add(new Pair<>(x ,center));
                }
                
                return list.stream();
            }
            
        };
    }

    @Override
    public <X> Graph<X> createFull(Set<X> nodes) {
        return new Graph<X>() {

            @Override
            public Set<X> getNodes() {
                Set<X> out = nodes.stream().collect(Collectors.toSet());
                return Collections.unmodifiableSet(out);
            }

            @Override
            public boolean edgePresent(X start, X end) {
                return start!=end;
            }

            @Override
            public int getEdgesCount() {
                return nodes.size()*2;
            }

            @Override
            public Stream<Pair<X, X>> getEdgesStream() {
                List<Pair<X,X>> list = new ArrayList<>();
                for(X x : nodes) {
                    Iterator<Pair<X, X>> str = createBidirectionalStar(x,nodes.stream()
                                .filter(n->!n.equals(x))
                                .collect(Collectors.toSet()))
                                .getEdgesStream()
                                .iterator();
                    while(str.hasNext()) {
                        list.add(str.next());
                    }
                }
                
                return list.stream();
            }
            
        };
    }

    @Override
    public <X> Graph<X> combine(Graph<X> g1, Graph<X> g2) {
        List<X> nodesTot = new ArrayList<>();
        g1.getNodes().forEach(e->nodesTot.add(e));
        g2.getNodes().forEach(e->nodesTot.add(e));
        List<X> nodes = new ArrayList<>(nodesTot.stream().distinct().collect(Collectors.toList()));
        return new Graph<X>() {
            
            @Override
            public Set<X> getNodes() {
                Set<X> out = nodes.stream().collect(Collectors.toSet());
                return Collections.unmodifiableSet(out);
            }

            @Override
            public boolean edgePresent(X start, X end) {
                return createDirectedStar(nodes.get(0),this.getNodes()
                        .stream().
                        filter(e->!e.equals(nodes.get(0)))
                        .collect(Collectors.toSet())).edgePresent(start, end);
            }

            @Override
            public int getEdgesCount() {
                return nodes.size()-1;
            }

            @Override
            public Stream<Pair<X, X>> getEdgesStream() {
                // TODO Auto-generated method stub
                return null;
            }
            
        };
    }

}
