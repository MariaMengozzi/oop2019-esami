package a01b.e2;

import javax.swing.*;
import java.util.*;
import java.util.Map.Entry;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Map<JButton, Pair<Integer, Integer>> jbs = new HashMap<>();
    private final int size;
    private final Logics logics;

    public GUI(int size, int mines) {
        this.logics = new LogicsImpl(size,mines);
        this.size = size;
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
        JPanel panel = new JPanel(new GridLayout(size, size));
        this.getContentPane().add(BorderLayout.CENTER, panel);
        ActionListener al = (e) -> {
            final JButton bt = (JButton) e.getSource();
            Pair<Integer,Integer> pos = jbs.get(bt);
            int value= logics.select(pos.getX(),pos.getY());
            String str =  value == -1 ? "*" : String.valueOf(value) ;
            bt.setText(str);
            bt.setEnabled(false);
            if (value == -1) {
                JOptionPane.showMessageDialog(null,"you Lose");
                System.exit(1);
            } else if (logics.win()) {
                JOptionPane.showMessageDialog(null,"you Win");
                System.exit(1);
            }
        };

        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                //final JButton jb = new JButton(String.valueOf(x) + " " + String.valueOf(y));
                final JButton jb = new JButton(" ");
                jb.addActionListener(al);
                this.jbs.put(jb, new Pair<>(x, y));
                panel.add(jb);
            }
        }
        this.setVisible(true);
    }


}
