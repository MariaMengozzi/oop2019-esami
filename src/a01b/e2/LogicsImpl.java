package a01b.e2;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

public class LogicsImpl implements Logics {
    private final int size;
    private final int nMines;
    private Map<Pair<Integer,Integer>,Integer> map = new HashMap<>();
    private Random rdm = new Random();
    private int countSel = 0;
    
    public LogicsImpl(int size, int mines) {
        this.nMines = mines;
        this.size = size;
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                this.map.put(new Pair<>(x, y),0);
            }
        }

        for (int i=0;i<mines;i++) {
            Pair<Integer,Integer> pos = new Pair<>(rdm.nextInt(size-1),rdm.nextInt(size-1));
            while(map.get(pos)==-1) {
                pos = new Pair<>(rdm.nextInt(size-1),rdm.nextInt(size-1));
            }
            map.put(pos, -1);
        }
        setNumMines();
        map.entrySet().stream().filter(e->e.getValue()==-1).forEach(System.out::println);
    }

    private void setNumMines() {
       for(Entry<Pair<Integer,Integer>,Integer> e : map.entrySet()) {
           if (e.getValue() != -1) {
               
           int count = 0;
           for (Entry<Pair<Integer,Integer>,Integer> x : map.entrySet()) {
                   if(near(x.getKey(),e.getKey()) && x.getValue() ==-1) {
                       count++;
                   }
               }
               map.replace(e.getKey(), count);
           }
           
       }
    }

    private boolean near(Pair<Integer, Integer> key, Pair<Integer, Integer> key2) {
        return Math.abs(key.getX()-key2.getX())<=1 && Math.abs(key.getY()-key2.getY())<=1;
    }

    @Override
    public int select(int x, int y) {
        if (map.get(new Pair<>(x,y))!=-1) {
            this.countSel++;
        }
        return map.get(new Pair<>(x,y));
    }

    @Override
    public boolean win() {
        return this.countSel == size*size-nMines;
    }

}
