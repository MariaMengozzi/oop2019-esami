package a01b.e1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class GridFactoryImpl implements GridFactory {
    @Override
    public <E> Grid<E> create(int rows, int cols) {
        return new GridImpl<E>(rows,cols);
    }
public class GridImpl<E> implements Grid<E> {
        
        private Map<Pair<Integer,Integer>,E> map = new HashMap<>();
        private List<Pair<Integer,Integer>> list = new ArrayList<>();
        private final int rows;
        private final int cols;
        
        public GridImpl(int r, int c) {
            this.rows = r;
            this.cols = c;
           for (int i=0;i<r;i++) {
               for (int j=0;j<c;j++) {
                   map.put(new Pair<>(i,j), null);
                   list.add(new Pair<>(i,j));
               }
           }
        }

        public int getRows() {
            return rows;
        }

        @Override
        public int getColumns() {
            return cols;
        }

        @Override
        public E getValue(int row, int column) {
            return map.get(new Pair<>(row,column));
        }

        @Override
        public void setColumn(int column, E value) {
            for(Entry<Pair<Integer, Integer>, E> e : map.entrySet()) {
                if(e.getKey().getY()==column) {
                    map.replace(e.getKey(), value);
                }
            }
        }

        @Override
        public void setRow(int row, E value) {
            for(Entry<Pair<Integer, Integer>, E> e : map.entrySet()) {
                if(e.getKey().getX()==row) {
                    map.replace(e.getKey(), value);
                }
            }
        }

        @Override
        public void setBorder(E value) {
            setColumn(0,value);
            setRow(0,value);
            setColumn(rows-1,value);
            setRow(cols-1,value);
        }

        @Override
        public void setDiagonal(E value) {
            for(Entry<Pair<Integer, Integer>, E> e : map.entrySet()) {
                if(e.getKey().getY()==e.getKey().getX()) {
                    map.replace(e.getKey(), value);
                }
            }
            
        }

        @Override
        public Iterator<Cell<E>> iterator(boolean onlyNonNull) {
            List<Cell<E>> out = new ArrayList<>();
            for(Pair<Integer,Integer> p : list) {
                out.add(new Cell<>(p.getX(),p.getY(),map.get(p)));
            }
            if(onlyNonNull) {
                out = out.stream().filter(e->e.getValue()!=null).collect(Collectors.toList());
            }
            return out.iterator();
        }
        
    }

}

 